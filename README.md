# Webová aplikace pro řešení třícestné merge kolize

## Mikuláš Dítě

https://mergephp.com/

- rozšíření pro git, slučování stromů, abstraktní syntaktický strom php, řešení kolize slučování, automatizace
- git extension, tree merging, PHP abstract syntax tree, collision resolution, automation

### Zdrojové soubory

Výstupy jsou ve větvy `medium` https://gitlab.fit.cvut.cz/ditemiku/bp/tree/medium.

Původní zdrojové soubory jsou v `master` https://gitlab.fit.cvut.cz/ditemiku/bp/tree/master.

Zdrojové kódy webové aplikace jsou ve větvy `frontend` https://gitlab.fit.cvut.cz/ditemiku/bp/tree/frontend.

Zdrojové kódy nového Merge algoritmu postaveného nad GumTree jsou v samostatném repozitáři na GitHubu: https://github.com/mikulas/gumtree/tree/php71-merge.

Git Plugin a dokumentace k němu se nachází také na GitHubu: https://github.com/Mikulas/gumtree-merge-driver.
