<?php declare(strict_types = 1);

require __DIR__ . '/../vendor/autoload.php';

umask(0007);

$root = dirname(__DIR__);
$params = [
	'rootDir' => $root,
	'srcDir' => "$root/src",
	'configDir' => "$root/config",
	'logDir' => "$root/log",
	'tempDir' => "$root/temp",
	'testDir' => "$root/tests",
];

$configurator = new Nette\Configurator;

$configurator->setDebugMode([
	'193.86.64.162', // office
]);
//$configurator->setDebugMode(TRUE);
//$configurator->setDebugMode(FALSE);

$configurator->addParameters($params);
$configurator->enableDebugger($params['logDir']);

$configurator->setTempDirectory($params['tempDir']);

$configurator->createRobotLoader()
	->addDirectory($params['srcDir'])
	->register();

$configurator->addConfig("$params[configDir]/config.neon");
$configurator->addConfig("$params[configDir]/local.neon");

$container = $configurator->createContainer();
$application = $container->getService('application');
assert($application instanceof Nette\Application\Application);
$application->catchExceptions = false;
$application->run();
