<?php declare(strict_types = 1);

namespace App;

use Mikulas\Diff3\ParsedSources;
use Mikulas\Diff3\Parser;
use Mikulas\Diff3\Tokenizer;


class Diff3
{

	private $parser;


	public function __construct()
	{
		$tokenizer = new Tokenizer();
		$this->parser = new Parser($tokenizer);
	}


	public function parse(string $diff3): ParsedSources
	{
		return $this->parser->parse($diff3);
	}

}
