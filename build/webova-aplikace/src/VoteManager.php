<?php declare(strict_types = 1);

namespace App;

use App\Model\MergeRequest;
use Nette\Http\Session;
use Nette\Http\SessionSection;


class VoteManager
{

	/** @var SessionSection */
	private $session;


	public function __construct(Session $session)
	{
		$this->session = $session->getSection(__CLASS__);
	}


	private function getKey(MergeRequest $request): string
	{
		return "voted-{$request->id}";
	}


	private function canVote(MergeRequest $request): bool
	{
		return !$this->session[$this->getKey($request)] ?? TRUE;
	}


	private function voted(MergeRequest $request)
	{
		$this->session[$this->getKey($request)] = TRUE;
	}


	public function upvote(MergeRequest $request): bool
	{
		if (!$this->canVote($request)) {
			return FALSE;
		}
		$request->upvote();
		$this->voted($request);
		return TRUE;
	}


	public function downvote(MergeRequest $request): bool
	{
		if (!$this->canVote($request)) {
			return FALSE;
		}
		$request->downvote();
		$this->voted($request);
		return TRUE;
	}

}
