<?php declare(strict_types = 1);

namespace App;

use Nette\Application\IRouter;
use Nette\Application\Routers\Route;
use Nette\Application\Routers\RouteList;


class RouterFactory
{

	public function create(): IRouter
	{
		$router = new RouteList();

		$router[] = new Route('diff3', 'Homepage:diff3');
		$router[] = new Route('files', 'Homepage:separateFiles');
		$router[] = new Route('submissions', 'Submissions:default');
		$router[] = new Route('result/<id>', 'Result:default');

		$router[] = new Route('<presenter>/<action>', 'Homepage:diff3');

		return $router;
	}

}
