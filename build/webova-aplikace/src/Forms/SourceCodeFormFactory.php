<?php declare(strict_types = 1);

namespace App\Forms;

use App\MergeRequester;
use Nette\Application\AbortException;
use Nette\Application\UI\Form;
use Nette\Forms\Controls\TextArea;
use Nette\Forms\Controls\TextInput;
use Nette\Forms\Controls\UploadControl;
use Nette\Http\FileUpload;
use Tracy\Debugger;


class SourceCodeFormFactory
{

	/** @var MergeRequester */
	private $requester;


	public function __construct(MergeRequester $requester)
	{
		$this->requester = $requester;
	}


	public function createDiff3(): Form
	{
		$form = new Form();

		$form->addUpload('file');
		$form->addTextArea('diff3');

		$form->onSuccess[] = function(Form $form) {
			try {
				$this->handleDiff3($form);
			} catch (\PhpParser\Error $exception) {
				Debugger::log($exception);
				$message = $exception->getMessage();
				$form->getPresenter()->flashMessage("You uploaded invalid PHP files: $message", 'error');
				return FALSE;
			} catch (\Exception $exception) {
				// ignore standard nette application flow
				if ($exception instanceof AbortException) {
					throw $exception;
				}

				Debugger::log($exception);
				$form->getPresenter()->flashMessage("Failed to merge, sorry about that", 'error');
				return FALSE;
			}
		};

		$form->addSubmit('send');
		return $form;
	}


	public function handleDiff3(Form $form) {
		$fileInput = $form['file'];
		assert($fileInput instanceof UploadControl);
		if ($fileInput->isOk()) {
			$file = $fileInput->getValue();
			assert($file instanceof FileUpload);
			$request = $this->requester->mergeDiff3File($file->getTemporaryFile());
		} else {
			$input = $form['diff3'];
			assert($input instanceof TextArea);
			$request = $this->requester->mergeDiff3Source($input->getValue());
		}
		$form->getPresenter()->redirect('Result:', ['id' => $request->id]);
	}


	public function createSeparateFiles(): Form
	{
		$form = new Form();

		$form->addUpload('fileA');
		$form->addUpload('fileB');
		$form->addUpload('fileBase');

		$form->addTextArea('sourceA');
		$form->addTextArea('sourceB');
		$form->addTextArea('sourceBase');

		$form->onSuccess[] = function(Form $form) {
		};

		$form->addSubmit('send');
		return $form;
	}


	private function merge(string $pathA, string $pathB, string $pathBase): string
	{
		return $this->requester->request($pathA, $pathB, $pathBase);
	}

}
