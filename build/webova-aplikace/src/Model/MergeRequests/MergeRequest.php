<?php declare(strict_types = 1);

namespace App\Model;

use Nette\Utils\Strings;
use Nextras\Orm\Entity\Entity;


/**
 * @property-read int    $id         {primary}
 * @property string $sourceA
 * @property string $sourceB
 * @property string $sourceBase
 * @property NULL|string $result
 * @property int $scoreSum
 * @property int $scoreVoters
 * @property \DateTime $createdAt
 */
class MergeRequest extends Entity
{

	public function __construct(string $sourceA, string $sourceB, string $sourceBase)
	{
		parent::__construct();
		$this->createdAt = new \DateTime();
		$this->sourceA = $sourceA;
		$this->sourceB = $sourceB;
		$this->sourceBase = $sourceBase;
		$this->scoreSum = 0;
		$this->scoreVoters = 0;
	}


	public function getScore(): ?float
	{
		if ($this->scoreVoters === 0) {
			return NULL;
		}
		return $this->scoreSum / $this->scoreVoters;
	}


	public function upvote()
	{
		$this->scoreSum++;
		$this->scoreVoters++;
	}


	public function downvote()
	{
		$this->scoreVoters++;
	}


	public function getLoC(): int
	{
		return max(count(explode("\n", $this->sourceA)), count(explode("\n", $this->sourceB)), count(explode("\n", $this->sourceBase)));
	}

}
