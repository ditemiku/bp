<?php declare(strict_types = 1);

namespace App\Model;

use Nextras\Orm\Mapper\Mapper;


class MergeRequestsMapper extends Mapper
{

	public function findRecent()
	{
		return $this->builder()
			->orderBy('created_at DESC')
			->limitBy(20);
	}

}
