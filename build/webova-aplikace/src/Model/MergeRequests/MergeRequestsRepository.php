<?php declare(strict_types = 1);

namespace App\Model;

use Nextras\Orm\Collection\ICollection;
use Nextras\Orm\Repository\Repository;


/**
 * @method ICollection|MergeRequest[] findRecent()
 */
class MergeRequestsRepository extends Repository
{

	public function create(string $sourceA, string $sourceB, string $sourceBase): MergeRequest
	{
		$request = new MergeRequest($sourceA, $sourceB, $sourceBase);
		$this->attach($request);
		return $request;
	}


	/**
	 * Returns possible entity class names for current repository.
	 *
	 * @return string[]
	 */
	public static function getEntityClassNames()
	{
		return [MergeRequest::class];
	}

}
