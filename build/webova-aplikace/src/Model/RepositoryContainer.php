<?php declare(strict_types = 1);

namespace App\Model;

use Nextras\Orm\Model\Model;


/**
 * @property-read MergeRequestsRepository $mergeRequests
 */
class RepositoryContainer extends Model
{

}
