<?php declare(strict_types = 1);

namespace App\Presenters;

use App\Model\MergeRequest;
use App\Model\MergeRequestsRepository;
use App\VoteManager;
use Nette\Application\UI\Presenter;


class ResultPresenter extends Presenter
{

	/** @var string @persistent */
	public $id;

	/** @var MergeRequest */
	private $request;

	/** @var MergeRequestsRepository */
	private $repo;

	/** @var VoteManager */
	private $voteManager;


	public function __construct(MergeRequestsRepository $repo, VoteManager $voteManager)
	{
		parent::__construct();
		$this->repo = $repo;
		$this->voteManager = $voteManager;
	}


	protected function startup()
	{
		parent::startup();

		$this->request = $this->repo->getById($this->id);
		if (!$this->request) {
			$this->error();
		}
	}


	public function renderDefault()
	{
		$this->template->request = $this->request;
	}


	public function handleDownload()
	{
		$filename = "merge-result-{$this->request->id}.php";
		$this->sendResponse(new SourceCodeResponse($this->request->result, $filename));
	}


	public function handleVoteUp()
	{
		if ($this->voteManager->upvote($this->request)) {
			$this->flashMessage('Thank you, your vote has been saved');
		}
		$this->repo->persistAndFlush($this->request);
		$this->redirect('this');
	}


	public function handleVoteDown()
	{
		if ($this->voteManager->downvote($this->request)) {
			$this->flashMessage('Thank you, your vote has been saved');
		}
		$this->repo->persistAndFlush($this->request);
		$this->redirect('this');
	}

}
