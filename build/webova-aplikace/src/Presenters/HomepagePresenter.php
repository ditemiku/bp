<?php declare(strict_types = 1);

namespace App\Presenters;

use App\Forms\SourceCodeFormFactory;
use Nette\Application\UI\Form;
use Nette\Application\UI\Presenter;


class HomepagePresenter extends Presenter
{

	/** @var SourceCodeFormFactory */
	private $diff3FormFactory;


	public function __construct(SourceCodeFormFactory $diff3FormFactory)
	{
		parent::__construct();
		$this->diff3FormFactory = $diff3FormFactory;
	}


	public function createComponentDiff3Form()
	{
		$form = $this->diff3FormFactory->createDiff3();
		$form->onSuccess[] = function(Form $form) {
			$this->redirect('this');
		};
		return $form;
	}


	public function createComponentSeparateFilesForm()
	{
		$form = $this->diff3FormFactory->createSeparateFiles();
		$form->onSuccess[] = function(Form $form) {
			$this->redirect('this');
		};
		return $form;
	}

}
