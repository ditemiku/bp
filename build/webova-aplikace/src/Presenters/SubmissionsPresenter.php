<?php declare(strict_types = 1);

namespace App\Presenters;

use App\Model\MergeRequestsRepository;
use Nette\Application\UI\Presenter;


class SubmissionsPresenter extends Presenter
{

	/** @var MergeRequestsRepository */
	private $repo;


	public function __construct(MergeRequestsRepository $repo)
	{
		parent::__construct();
		$this->repo = $repo;
	}


	public function renderDefault()
	{
		$this->template->recentSubmissions = $this->repo->findRecent();
	}


}
