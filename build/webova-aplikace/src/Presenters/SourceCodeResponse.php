<?php declare(strict_types = 1);

namespace App\Presenters;

use Nette;
use Nette\Application\IResponse;


class SourceCodeResponse implements IResponse
{

	/** @var string */
	private $content;

	/** @var string */
	private $filename;


	public function __construct(string $content, string $filename)
	{
		$this->content = $content;
		$this->filename = $filename;
	}


	public function send(Nette\Http\IRequest $httpRequest, Nette\Http\IResponse $httpResponse): void
	{
		$httpResponse->setContentType('text/plain', 'utf-8');
		$httpResponse->addHeader('Content-Transfer-Encoding', 'Binary');
		$httpResponse->addHeader('Content-Disposition', "attachment; filename={$this->filename}");

		echo $this->content;
		echo "\n";
	}

}
