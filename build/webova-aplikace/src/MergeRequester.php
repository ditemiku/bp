<?php declare(strict_types = 1);

namespace App;

use App\Model\MergeRequest;
use App\Model\MergeRequestsRepository;
use Nette\Utils\FileSystem;


class MergeRequester
{

	/** @var MergeRequestsRepository */
	private $repo;

	/** @var Diff3 */
	private $diff3Parser;

	/** @var GumtreeMerger */
	private $appMerger;


	public function __construct(MergeRequestsRepository $repo, Diff3 $diff3, GumtreeMerger $appMerger)
	{
		$this->repo = $repo;
		$this->diff3Parser = $diff3;
		$this->appMerger = $appMerger;
	}


	public function mergeDiff3File(string $path): MergeRequest
	{
		$diff3 = FileSystem::read($path);
		return $this->mergeDiff3Source($diff3);
	}


	public function mergeDiff3Source(string $diff3): MergeRequest
	{
		$sources = $this->diff3Parser->parse($diff3);
		return $this->mergeSources($sources->getOurs(), $sources->getTheirs(), $sources->getBase());
	}


	public function mergeFiles(string $pathA, string $pathB, string $pathBase): MergeRequest
	{
		return $this->mergeSources(
			FileSystem::read($pathA),
			FileSystem::read($pathB),
			FileSystem::read($pathBase)
		);
	}


	public function mergeSources(string $a, string $b, string $base): MergeRequest
	{
		$request = $this->repo->create($a, $b, $base);
		$request->result = $this->appMerger->merge($a, $b, $base);
		$this->repo->persistAndFlush($request);
		return $request;
	}

}
