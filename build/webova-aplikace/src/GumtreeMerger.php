<?php declare(strict_types = 1);

namespace App;

use Nette\Utils\FileSystem;
use Symfony\Component\Process\ProcessBuilder;
use Tracy\Debugger;


class GumtreeMerger
{

	/** @var string */
	private $gumtreeBin;

	/** @var string */
	private $javaHome;


	public function __construct(string $gumtreeBin, string $javaHome)
	{
		$this->gumtreeBin = $gumtreeBin;
		$this->javaHome = $javaHome;
	}


	public function merge(string $a, string $b, string $base): string
	{
		$fileA = $this->createFile($a);
		$fileB = $this->createFile($b);
		$fileBase = $this->createFile($base);

		$builder = new ProcessBuilder([$this->gumtreeBin, 'merge', $fileBase, $fileA, $fileB]);
		$builder->setEnv('JAVA_HOME', $this->javaHome);
		$process = $builder->getProcess();
		$process->mustRun();

		$output = $process->getOutput();

		FileSystem::delete($fileA);
		FileSystem::delete($fileB);
		FileSystem::delete($fileBase);

		return $output;
	}


	private function createFile(string $a): string
	{
		$name = tempnam(sys_get_temp_dir(), 'merge-') . '.php';
		FileSystem::write($name, $a);
		return $name;
	}

}
