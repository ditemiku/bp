CREATE TABLE merge_requests (
  id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  source_a TEXT NOT NULL,
  source_b TEXT NOT NULL,
  source_base TEXT NOT NULL,
  result TEXT NULL,
  score_sum INT NULL,
  score_voters INT NULL,
  created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
);
