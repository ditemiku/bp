# https://mergephp.com/

## Project setup

1. Create database 
```
CREATE DATABASE `bp` COLLATE 'utf8mb4_general_ci';
```

2. Update `local.neon` with your database credentials.

3. Run database migrations
```
php www/index.php migrations:continue
```
