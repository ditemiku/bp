Při programování v týmu je často potřeba začlenit do zdrojových kódů dvě kolizní změny. Současné algoritmy vycházejí z porovnávání řádků a jsou rychlé. S nárůstem výpočetního výkonu jsou ale nyní k dispozici i algoritmy sofistikovanější, které zvládnou automaticky vyřešit víc konfliktů.

Implementací algoritmů pracujících nad stromy se programátorům usnadní začleňování změn zdrojových souborů. Ruční řešení kolizí je zdlouhavé a náchylné na vznik těžko detekovatelných chyb, čemuž chceme předejít.

Jako vývojář na tento problém narážím denně, stejně tak moji kolegové. Automatizace programátorům nejenom ušetří práci, ale především usnadní a urychlí začleňování změn od jednotlivých autorů do sdíleného repozitáře.
