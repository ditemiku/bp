% Obsah přiloženého média

Identická data jako na přiloženém médiu jsou také dostupná z url\newline
\url{https://gitlab.fit.cvut.cz/ditemiku/bp/tree/medium/build}.

\vfill

\begin{dirfigure}%
	\dirtree{%
		.1 README.md\DTcomment{stručný popis obsahu média}.
		.1 tex.
			.2 desky\DTcomment{zdrojová forma desek práce}.
			.2 prace\DTcomment{zdrojová forma práce ve formátu Markdown a \XeLaTeX{}}.
		.1 git-plugin\DTcomment{skripty a dokumentace rozšíření verzovacího systému Git}.
		.1 gumtree\DTcomment{zdrojové soubory Java aplikace s AST Merge algoritmem}.
		.1 webova-aplikace\DTcomment{zdrojové soubory webové PHP aplikace}.
		.1 BP\_Dite\_Mikulas\_2017.pdf\DTcomment{text práce ve formátu PDF}.
		.1 desky.pdf\DTcomment{desky práce ve formátu PDF}.
	}
\caption{Obsah přiloženého média}
\end{dirfigure}
