Byly zdokumentovány diff a merge algoritmy v soudobých open-source verzovacích nástrojích. Z velké části musely být informace čerpány přímo ze zdrojových kódů. Dále byl představen nový směr, kterým se řešení této problematiky podle posledních článků ubírá, mapování stromových struktur. Byly představeny dvě konkrétní implementace tohoto přístupu: \textsc{3dm} a \textit{GumTree}.

Byl navržen nový algoritmus řešící třícestný merge s využitím existujících programů pro mapování uzlů mezi dvěma stromy. Tento algoritmus byl implementován v jazyku Java. Z porovnání generovaných řešení s verzovacím nástrojem Git vyšel tento algoritmus jako přesnější (generující méně kolizí).

Pro snadné praktické použití vznikl oproti zadání navíc Git plugin, který nový merge transparentně zapojuje.

Dále byla navrhnuta cloudová architektura s vysokou dostupností, webová aplikace a uživatelské rozhraní, a práce byla publikována na internetu na adrese \url{https://mergephp.com/}.

Práci lze dále rozšiřovat přidáním podpory pro zdrojové soubory jiných programovacích jazyků než je PHP. Implementace je naprogramována obecně a postup pro přidání dalších jazyků je zdokumentovaný. Zajímavá by rovněž byla analýza přístupu, kde se budou aplikovat rozdílná merge pravidla podle konkrétního AST kontextu (například jiná pravidla pro insert v kontextu třídy a pro parametry metody). Pro lepší integraci s verzovacím systémem by bylo vhodné analyzovat diff a vypisovat vysokoúrovňové změny (například „metoda \code{sum} byla přesunuta do třídy \code{Numeric}“).
