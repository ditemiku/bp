# Načtení souborů a parsování do AST

Tento krok je triviální a záleží především na jazyku, jehož zdrojové kódy se načítají. Stačí implementovat pouze jeden \textsc{xml} parser a AST ostatních jazyků předpočítat samostatně a předávat serializované do \textsc{xml}.

Výsledkem tohoto kroku jsou tři AST. V dalším kroku se budou uzly těchto AST navzájem porovnávat a mapovat.

Časová komplexita záleží na konkrétním použitém parseru. Pro \textit{Adaptive LL(*)} je $O(n^4)$ (kde $n$~je počet AST uzlů), ale pro typické vstupy je parsování lineární~\cite{Parr2014}.

# Mapování uzlů mezi AST

\iffalse
\begin{figure}[H]
  \includegraphics[width=\textwidth,scale=0.5]{images/bp-ast-matching}
  \caption{Příklad mapování uzlů mezi dvěma AST. Levá část popisuje kód \code{return 1+1}, pravá část kód \code{return \$a + \$b}. Uzly propojené zelenou šipkou jsou na sebe vzájemně mapované. Červené a zelené uzly nemají žádné mapování.}
\end{figure}
\fi

Detailní fungování tohoto algoritmu je popsáno \hyperref[gumtree]{v kapitole}. V integraci mého řešení nebyl s tímto GumTree modulem žádný problém.

Tento algoritmus běží nejlépe v čase s $O(n^3)$, kde $n$ je počet AST uzlů \cite{Pawlik2011}.

# Spojení tří mapovaných AST do jednoho
\label{spojovani-ast}

Následující algoritmus je hlavním přínosem této práce. Popisuje, jak spojit tři AST se vzájemně namapovanými uzly a detekovat kolize. Nejprve definuji pomocné funkce:

\begin{listing}[H]
  \begin{algorithmic}
    \Function{findParent}{$left, right$}
      \If{$\exists$ any mapping $m$ from $left$ to $base$}
        \State
        \Return $m$
      \ElsIf{$\exists$ any mapping $m$ from $right$ to $base$}
        \State
        \Return $m$
      \Else
        \State
        \Return empty node
      \EndIf
    \EndFunction
  \end{algorithmic}
\end{listing}

Tento pomocný algoritmus nalezne vhodný uzel z $base$. Pokud neexistuje, což se stane
když je $left$ nebo $right$ nově vložený uzel, vrací prázdný uzel.

\begin{listing}[H]
  \begin{algorithmic}
    \Function{findDeleted}{$base, side$}
      \State $c \leftarrow$ children of $side$
      \ForAll{$child \leftarrow$ children of $base$}
        \If{$\nexists$ mapping of $child$ to $c$}
          \State yield $child$
        \EndIf
      \EndFor
    \EndFunction
  \end{algorithmic}
\end{listing}

Vrací přímé potomky $base$, pro které neexistuje vhodné mapování na přímé potomky $side$. Jsou to uzly, které existovaly v $base$, ale ne v $side$, a ve výsledném merge by se neměly objevit.

\begin{listing}[H]
  \begin{algorithmic}
    \Function{movedNodes}{$base, side, deleted$}
      \State $base \leftarrow base \not\ $\hspace{1ex} deleted
      \State $side \leftarrow side \not\ $\hspace{1ex} deleted
      \State $side \leftarrow side \not\ $\hspace{1ex}$\{child \in side : \nexists$ mapping of $child$ to $base\}$

      \State $indexes \leftarrow $ find index pairs: index in $base$ and index in $side$
      \State
      \Return nodes which index decreased
    \EndFunction
  \end{algorithmic}
\end{listing}

Nejprve se z $base$ odstraní smazané uzly. Z $side$ se odstraní smazané uzly a uzly nové (vkládané). Dále se pracuje pouze s uzly, které se vyskytují ve všech třech stromech (přestože funkce přijímá pouze $base$ a jednu $side$, proměnná $deleted$ obsahuje i uzly smazané v třetím stromu). Pro každý ze zbylých uzlů se najde původní a nová pozice. Pokud se pozice snížila, je uzel označen jako posunutý a vrácen. Příklad: pro uzly $base = \{a, b\}$ a $side = \{b, a\}$ se uzel $a$ přesunul z pozice $1$ na pozici $2$ a tedy není označen jako přesunutý. Uzel $b$ se z $2$ přesunul na $1$ a funkce ho tedy vrátí.

Označování přesunutých uzlů pouze v jednom směru umožňuje spojovat změny uvnitř přesunu bez detekování falešně pozitivní kolize. Například pro $base~=~\{a, x, z,b\}$, $left = \{b, x, z,a\}$, $right = \{a, x, Y, z,b\}$ se v části $left$ vyhodnotí jako přesunuté pouze $\{b\}$, a ne celá sekvence $\{a, x, z, a\}$ a lze bez kolize zapojit změny z $right$. Detailněji tento případ rozeberu níže u systému zamykání.

\begin{listing}[H]
  \begin{algorithmic}
    \Function{makeMergeList}{$base, side, deleted$}
      \State $list \leftarrow \{\}$
      \State insert start marker (empty node) to $list$
      \ForAll{$child \leftarrow$ children of $side$}
        \If{$\nexists$ mapping of $child$ to $base$}
          \Comment $child$ is new (inserted) node
          \State mark top of $list$ as right locked
          \State right lock $child$
        \ElsIf{$child \in $ \Call{movedNodes}{$base, side, deleted$}}
          \State mark top of $list$ as right locked
          \State right lock $child$
        \EndIf
        \State insert $child$ to $list$
      \EndFor
      \State
      \Return $list$
    \EndFunction
  \end{algorithmic}
\end{listing}

Funkce vrací efektivně stejný seznam jako je $side$, ale rozšířený o zámky. Aby se zachoval uživatelův záměr, při vkládání nového uzlu se zamknou uzly na obou stranách. Tato trojice je pak nerozdělitelná a pokud by ji změny z druhého stromu rozbily, dojde ke kolizi. Seskupování uzlů je vyřešeno pravostranným zámkem; levý zámek by fungoval identicky, ale místo označení počátku by bylo nutné pracovat s označením konce a implementace by byla méně elegantní. Tato část algoritmu je podobná Lindholmovu \cite{Lindholm2001}, ale je zjednodušena (odstranění levostranného zámku, \textit{hang-ons}) a logika pro zamykání je jiná.

\begin{algorithmic}
  \Function{createNode}{$base, left, right$}
    \If{type of $left = $ type of $right$}
      % \State \Comment{either same as $base$ or both changed equally}
      \State $type \leftarrow$ common type of $left$ and $right$

      \If{content of $left = $ content of $right$}
        \State $content \leftarrow$ content of $left$
      \ElsIf{content of $base = $ content of $right$}
        % \State \Comment{$right$ is not changed, $left$ may differ}
        \State $content \leftarrow$ content of $left$
      \ElsIf{content of $base = $ content of $left$}
        % \State \Comment{$right$ is not changed, $right$ may differ}
        \State $content \leftarrow$ content of $right$
      \Else
        % \State \Comment{both sides changed differently}
        \State conflict
      \EndIf

    \ElsIf{type of $base = $ type of $right$}
      % \State \Comment{$right$ is not changed, $left$ may differ}
      \State $type \leftarrow$ type of $left$
      \State $content \leftarrow$ content of $left$

    \ElsIf{type of $base = $ type of $left$}
      % \State \Comment{$left$ is not changed, $right$ may differ}
      \State $type \leftarrow$ type of $right$
      \State $content \leftarrow$ content of $right$

    \Else
      % \State \Comment{both sides changed differently}
      \State conflict
    \EndIf

    \State
    \Return node of type $type$ with content $content$
  \EndFunction
\end{algorithmic}

\begin{figure}[H]
  \includegraphics[width=0.6\textwidth]{images/bp-locking}
  \caption{Ukázka pravostranného zamykání uzlů. Znak $\land$\ reprezentuje označení počátku (\textit{start marker}). V $left$ ze prohodily uzly $a$ a $b$, v $right$ byl vložen nový uzel $y$ mezi $x$ a $z$. Červeně jsou znázorněny semknuté uzly, které musí být u sebe a nelze je rozdělit bez ztráty záměru. Protože se zámky nepřekrývají, tyto dvě změny lze začlenit bez kolize.}
\end{figure}

Tento algoritmus vytváří nový uzel ze tří vstupních uzlů. Protože spojujeme na úrovni uzlů, jakákoliv neshoda na této úrovni vyústí v nevyřešitelný konflikt. Prakticky toto bude nastávat pouze u dlouhých řetězců (ukázka je \hyperref[sample-b]{v příloze}). Pro ty by bylo vhodné implementovat například samostatný merge na úrovni slov oddělených mezerou. Typem uzlu se rozumí \textit{parser context}, tedy například \code{PhpBlock}, \code{AdditionContext} a podobně. Obsah (\code{content}) uzlu je například `ahoj` pro řetězec.

\phantomsection
\label{merge-ztrata-zmen}
Na tomto místě by také bylo vhodné varovat před ztrátou změn. Pokud $right$ má stejný $type$ jako $base$ a má pouze nějaké změny v $content$, $left$ změnou $type$ tyto úpravy přepíše. Velmi striktní merge strategie by na tomto místě měla vyhodit kolizi.

\clearpage
\begin{listing}[H]
  \begin{algorithmic}
    \Function{merge}{$base, left, right$}
      \State $deleted \leftarrow\ $ \Call{findDeleted}{$base, left$}$\ \cup\ $\Call{findDeleted}{$base, right$}
      \State $list_l \leftarrow\ $\Call{makeMergeList}{$base, left, deleted$}
      \State $list_r \leftarrow\ $\Call{makeMergeList}{$base, right, deleted$}

      \State $cur_l \leftarrow$ iterator on $list_l$
      \State $cur_r \leftarrow$ iterator on $list_r$

      \State $mergedTree \leftarrow$ \Call{createNode}{$base, left, right$}

      \While{$cur_l$ is valid $\lor\ cur_r$ is valid}
        \If{$\exists$ mapping between $left$ and $right$}
          \Comment locking is irrelevant
          \State $parent \leftarrow$ findParent$(cur_l, cur_r)$
          \State append \Call{merge}{$parent, cur_l, cur_r$} to $mergedTree$
          \State advance $cur_l$
          \State advance $cur_r$

        \ElsIf{previous $cur_l$ is locked $\land$ previous $cur_r$ is locked}
          \If{$\nexists$ mapping between $left$ and $right$}
            \State conflict
          \EndIf
          \State $parent \leftarrow$ findParent$(cur_l, cur_r)$
          \State append \Call{merge}{$parent, cur_l, cur_r$} to $mergedTree$
          \State advance $cur_l$
          \State advance $cur_r$

        \ElsIf{previous $left$ was locked}
          \State append $left$ (including child nodes) to $mergedTree$
          \State advance $cur_l$

        \ElsIf{previous $right$ was locked}
          \State append $right$ (including child nodes) to $mergedTree$
          \State advance $cur_r$

        \Else
          \Comment asserting $left$ and $right$ map to each other
          \State $parent \leftarrow$ findParent$(cur_l, cur_r)$
          \State append \Call{merge}{$parent, cur_l, cur_r$} to $mergedTree$
          \State advance $cur_l$
          \State advance $cur_r$
        \EndIf

      \EndWhile
      \State
      \Return $mergedTree$
    \EndFunction
  \end{algorithmic}
\end{listing}

Tato funkce je samotné jádro mého merge algoritmu. Nejprve se sestaví \textit{merge list} pro obě strany. Poté se paralelně prochází potomci obou stran. Pokud mezi aktuálním levým a pravým potomkem existuje mapování, rekurzivně se spočte jejich spojení, jinak se využije systému zámků. Jsou-li oba předchozí uzly zamknuté, současné uzly se musí shodovat, jinak nastává kolize (jedna ze semknutých ntic by se musela rozdělit). Pokud je zamknutý pouze jeden uzel, jedná se o přemístění nebo vložení, které není v druhé větvi. Poslední možnost nastává, pokud se uzly implicitně shodují, ale v okolí nedošlo k žádné změně, která by vytvořila zámky. Pokud by se uzly neshodovaly, jedna ze stran by byla označena jako odstranění a druhá jako vkládání a došlo by k vytvoření zámků.

Pokud je pouze jeden z kurzorů $cur_l$ nebo $cur_r$ je nevalidní, došlo k vkládání nového uzlu na konec. Všechny funkce s tím musí počítat a mít stejný efekt, jako kdyby místo nevalidního kurzoru dostaly prázdný strom.

Algoritmus \code{merge} má časovou komplexitu $O(n)$, kde $n$ je počet uzlů v AST.

\hyphenpenalty=10000

# Transformace spojeného AST zpět do kódu

\hyphenpenalty=0
\label{mapovani-na-kod}

Převedení syntaktického stromu zpět do kódu lze dvěma způsoby.

První z nich je pomocí nějakých pravidel každý uzel transformovat do kódu. Tím se ztratí původní formátování a často i čitelnost. Například číslo původně zapsané jako `1e6` by se bez složitých pravidel vypsalo `100000.0`, protože PHP parser informace o původním formátu zahazuje. Podobně by se jinak vypsal string interpolation, místo \code{"ahoj \$name"} by se vypsalo \code{"ahoj \{\$name\}"}. Ve všech případech jde o validní a funkčně identický kód, ale jiný, než napsal uživatel. Výhodou tohoto přístupu je jednotné formátování všeho kódu. Nemyslím si ale, že toto je úkolem verzovacího systému.
\iffalse
\footnote{Možná se někdy dočkáme jazyka, který místo plaintextu má zdrojové soubory reprezentované přímo nějakou serializací AST. Vyžadoval by ekosystém podpůrných nástrojů jako je podpora v IDE, verzovacích nástrojích a podobně. Odpadly by války o formátování kódu a každý vývojář by si nastavil formát, který jim vyhovuje. Současné automatické formátování není řešení, protože přeformátovaný kód nelze vhodně verzovat.}
\fi

Druhou možností je namapování výstupních AST uzlů na původní zdrojové kódy. Dílčí tokeny budou naformátované vždy tak, jak je napsal uživatel. U příkladu výše by ve výsledném souboru bylo opravdu `1e6`, pokud to tak uživatel zadal na vstupu. Menší problém tvoří bílé znaky, které jsou pro čitelnost kódu velmi důležité (díky odřádkování se typicky seskupují související příkazy a podobně). Parser typicky bílé znaky zahazuje a při spojování vstupních AST s nimi tedy nemůžeme pracovat.

Následující algoritmus mapuje AST na vstupní zdrojové kódy. Očekává AST, které má všechny tisknutelné uzly v listech.

\begin{listing}[H]
  \begin{algorithmic}
    \Function{printResult}{$tree, src_l, src_r$}
      \If{$tree$ is from left side}
        \State $src \leftarrow src_l$
      \Else
        \State $src \leftarrow src_r$
      \EndIf

      \If{$tree$ is leaf}
        \State $start \leftarrow$ end of token previous to tree
        \State $end \leftarrow$ end of tree token
        \State $gist \leftarrow$ \Call{substring}{from $start$ to $end$ of $tree$ content}
        \State \Call{print}{$gist$}

      \Else
        \ForAll{$child \leftarrow$ children of $tree$}
          \State
          \Call{printResult}{$child, src_l, src_r$}
        \EndFor
      \EndIf
    \EndFunction
  \end{algorithmic}
\end{listing}

Využíváme toho, že \textsc{antlr} parser vrací pro každý token mapování na pozice v zdrojovém souboru. Při merge si tuto informaci uložíme a bychom s ní zde mohli pracovat. U každého uzlu si také uchováme informaci, jestli byl vygenerován z levého nebo pravého stromu (a tedy z kterého zdrojového souboru). Nemůže být z obou zároveň, protože by došlo při merge ke kolizi.

Důležitý je zde trik, jak obejít chybějící whitespace v parseru. Místo vypsání uzlu od prvního namapovaného znaku začneme zdrojový kód vypisovat už od konce tokenu předešlého. Tím vypíšeme i bílé znaky, které parser zahodil.

Tato strategie ztrácí některé whitespace změny, ale výstupní formát je vždy stejný, jako v části vstupních souborů. Protože výsledný kód je vždy validní, tyto kolize nejsou detekovány a nevyhazují kolize.

Tento algoritmus má časovou komplexitu $O(n)$, kde $n$ je počet uzlů v AST.
