% Dokumentace Git pluginu
\label{plugin-doc}

Tato příloha popisuje postup instalace rozšíření pro Git, jehož implementace je popsána \hyperref[git-plugin]{v kapitole}. Odkazované zdrojové kódy jsou na přiloženém médiu a online \footnote{\url{https://github.com/Mikulas/gumtree-merge-driver}}.

### Požadavky a závislosti

\begin{itemize}
  \item \code{Java SE JRE}, alespoň ve verzi \code{1.7.0},
  \item \code{bash} interpreter,
  \item \code{Git}, alespoň ve verzi \code{v1.5.2}.
\end{itemize}

\clearpage

### Postup instalace

\begin{enumerate}
  \item Získejte aplikaci GumTree buď z přiloženého média, nebo z posledního dostupného online vydání \footnote{\url{https://github.com/Mikulas/gumtree/releases/}}.
  \item Archiv rozbalte na lokální disk a zapamatujte si cestu ke spustitelnému souboru \code{gumtree}.
  \item Následují změny se aplikují na konkrétní Git repozitář, ve kterém chcete GumTree merge používat:
  \begin{enumerate}
    \item Aktualizujte konfigurační soubor \code{.git/config} přidáním tohoto ústřižku. Vyplňte reálnou cestu k \code{gumtree}.
    \begin{minted}{ini}
[merge "gumtree"]
name = GumTree merge driver
driver = .git/gumtree-driver %O %A %B "cesta-k-gumtree"
recursive = binary
    \end{minted}

    \item Zkopírujte samotný \textit{merge driver} (soubor \code{.git|merge-driver}) do \code{.git/merge-driver} a nastavte ho jako spustitelný příkazem \code{chmod a+x .git/merge-driver}.
    \item Zapněte \textit{merge driver} pro konkrétní přípony v souboru \code{.gitattributes} přidáním tohoto ústřižku:
    \begin{minted}{ini}
*.php merge=gumtree
    \end{minted}

  \end{enumerate}
\end{enumerate}


### Použití

Toto rozšíření je naprosto transparentní a uživatel nemusí kromě počáteční instalace měnit své zaběhlé postupy. Pokud se při volání příkazu \code{git merge} detekuje kolize nad php soubory, git zavolá nastavený GumTree merge.

\clearpage

### Zdrojový soubor

\inputminted[frame=single,fontsize=\footnotesize,breaklines=true]{bash}{../merge-driver}
