Děkuji Ing. Vojtěchu Jirkovskému za vedení této práce, Ing. Miroslavu Hrončokovi za tipy k psaní práce a \XeLaTeX\ šablonu, a Prof. Ing. Adam Heroutovi, Ph.D za články o psaní diplomové práce.
