# Architektura webové aplikace a použitá infrastruktura

Webová aplikace, kterou má tato práce za cíl vytvořit, je z uživatelského hlediska jednoduchá a všechna těžká práce probíhá na pozadí, bez interakce s uživatelem.

Specifikace webové aplikace z pohledu uživatele:

\begin{itemize}
  \item Na úvodní stránce je popis aplikace a přímo formulář pro nahrání zdrojových souborů.
  \item Výchozí formát je `diff3`, lze přepnout tabem (stejná HTML stránka) na nahrávání tří samostatných souborů.
  \item Po odeslání formuláře se přímo zobrazuje stránka s výsledkem merge. Kromě samotných výsledků je zde možnost ohodnotit kvalitu výsledku.
  \item Všechny výsledky a jejich hodnocení se ukládají pro pozdější zpracování. Jejich seznam (historie řešení) je na samostatné stránce.
\end{itemize}

Pro implementaci webového rozhraní jsem zvolil jazyk `PHP 7.1` \cite{php} a aplikační framework `Nette 2.4` \cite{nette}. Jako databázovou vrstvu jsem zvolil Amazon Aurora: proprietární databázový systém v cloudové infrastruktuře \textit{Amazon Web Services} (AWS) kompatibilní s MySQL. Aplikace poběží ve virtuálním serveru taktéž v AWS.

Framework `Nette` jsem vybral, protože s ním mám největší praktické zkušenosti a oproti Symfony nabízí větší uživatelský komfort. Další webové PHP frameworky jako například Laravel jsem neuvažoval, protože nejsou dostatečně rozšířené, zdokumentované, nebo nepoužívají základní návrhové vzory jako je například \textit{Dependency injection} a špatně se rozšiřují a testují.

Tradiční web hosting nebyl pro tuto aplikaci použitelný, protože musíme spouštět Java aplikaci GumTree. Druhý extrém, vlastní dedikovaný server, byl rovněž zavržen, protože je zbytečně drahý. Cloudové řešení bylo vybráno jako dobrý kompromis kontroly nad systém a cizí správa a na rozdíl od jedné VPS (virtuální privátní server) lze dosáhnout vyšší dostupnosti (HA, \textit{high availability}). Z cloudových řešení mám praktické zkušenosti pouze s AWS.

Použitá architektura (diagram \ref{fig:arch}) je typická pro aplikace vyžadující vysokou dostupnost, kde load balancer přeposílá rovnoměrně požadavky mezi více webových (aplikačních) serverů, pro které výpadek jednoho z nich neznamená nedostupnost. Podobně je redundantní i databáze, která se aktivně používá pouze jedna, ale souběžně běží replika, která je připravena kdykoliv v případě problému nahradit master. Oba aplikační servery i databázové repliky jsou ve fyzicky jiných místech (v terminologii AWS \textit{availability zones}). Redis slouží jako sdílené úložiště pro sezení (\textit{sessions}).

Strukturu webové aplikace a zpracování \textsc{http} požadavku nad frameworkem Nette znázorňují diagramy \ref{fig:web-a} a \ref{fig:web-b}, kde je také zdokumentováno propojení Java aplikace a uživatelského rozhraní v PHP.

\begin{figure}
  \begin{adjustwidth}{-1.5in}{-1.5in}
    \centering
    \includegraphics[width=1.3\textwidth,scale=0.5]{images/bp-infra}
  \end{adjustwidth}
  \caption{Vysokoúrovňový pohled na použitou infrastrukturu.}
  \label{fig:arch}
\end{figure}

\begin{figure}[H]
  \includegraphics[width=\textwidth,scale=0.5]{images/bp-server-infra}
  \caption{Zpracování HTTP požadavku na jednom aplikačním serveru. NGINX se také stará o zabezpečení pomocí TLS (probíhá zde tedy tzv. \textit{TLS termination}), což na diagramu není znázorněno.}
\end{figure}

\begin{figure}[H]
  \begin{adjustwidth}{-1.5in}{-1.5in}
    \centering
    \includegraphics[width=1.3\textwidth,scale=0.5]{images/bp-app}
  \end{adjustwidth}
  \caption{Architektura webu z pohledu vývojáře. Průchod požadavku Nette aplikací, stejný pro všechny aplikace. Byznys logika je implementovaná ve službách, které se volají z \code{Presenteru}.}
  \label{fig:web-a}
\end{figure}

\begin{figure}[H]
  \includegraphics[width=\textwidth,scale=0.5]{images/bp-app-merge}
  \caption{Hlavní byznys logika aplikace zajišťující vyřešení kolize zdrojových souborů, které se načtou z formuláře. Na obrázku je rozkreslen i detail merge procesu v \code{GumtreeMerger} z pohledu webové aplikace, kde se volá spustitelný soubor v jazyku Java.}
  \label{fig:web-b}
\end{figure}


### Uživatelské rozhraní

Rozhraní bylo bez předchozího navrhování a plánování přímo implementováno. Bylo dostatečně jednoduché a z předchozích zkušeností celkem jednoznačně dané. Hlavní use case - nahrání kolizního stavu k řešení v \code{diff3} – je přímo na homepage. Možnost nahrát samostatně každý soubor je vzdálena jedno kliknutí za záložkou. Podle statistik používání lze tyto dvě možnosti prohodit. Pokud budou obě varianty používány podobně často, bylo by vhodné taby zrušit a oba formuláře zobrazit na jedné stránce, například vedle sebe. Současná implementace ale myslí i na to, aby uživatel nebyl zahlcen vstupními poli a dokázal formulář snadno vyplnit.

Po odeslání formuláře se uživateli buď zobrazí chybová hláška (například pokud zadané soubory nejsou validní PHP), nebo je přesměrován na stránku s výsledkem. Přestože se zobrazují čtyři zdrojové kódy – báze, levá a pravá strana a výsledek – bylo cílem je uživateli prezentovat přehledně.

Uživatelské rozhraní – konkrétně stránka s výpisem výsledku – je \hyperref[ukazky-webu]{v příloze}.

### Publikování aplikace

Webová aplikace je veřejně dostupná online na internetu\newline
a to na adrese \url{https://mergephp.com/}.
