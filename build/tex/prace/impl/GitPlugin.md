# Git plugin
\label{git-plugin}

Pro praktické uplatnění v praxi bylo vytvořeno rozšíření pro verzovací systém Git. Po instalaci uživatel může automaticky provádět merge a pro vhodné soubory se transparentně zavolá GumTree merge z této práce.

Jedna z možností byla přidat vlastní \textit{merge tool}, ale ten není dostatečně automatický. Uživatel ho musí ručně vyvolat příkazem `git mergetool` nad kolizním stavem. Další možností bylo vytvořit novou \textit{merge strategy}, která se volá vždy, když se spojují dva commity. To by ale obnášelo duplikaci hodně logiky, kterou má Git zabudovanou.

Místo toho byl implementován vlastní \textit{merge driver} \cite{git:doc:merge-driver}. Jde o skript, který Git zavolá, když narazí na dva kolizní soubory. Zdrojové kódy jsou na přiloženém médiu a také dostupné online\footnote{\url{https://github.com/Mikulas/gumtree-merge-driver}}. Dokumentace k instalaci je kromě toho také \hyperref[plugin-doc]{v příloze}.
