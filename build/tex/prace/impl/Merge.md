# Merge algoritmus

Vlastní merge algoritmus popsaný \hyperref[myslenky]{v kapitole} jsem implementoval jako nový modul v aplikaci GumTree. Převedení algoritmu do zdrojového kódu v jazyku Java se obešlo bez pozoruhodných komplikací. Funkční aplikace je na přiloženém médiu, online\footnote{\url{https://github.com/Mikulas/gumtree/releases/}} a může být začleněna do oficiálního repozitáře GumTree.

Jako základ celého projektu jsem využil implementaci GumTree \cite{gumtree:src}. Je dostupná pod open-source licencí LGPL-3.0 \cite{gumtree:src:license}. Každá část projektu je implementována jako samostatný modul, což zvyšuje čitelnost, rozšiřitelnost a naopak snižuje nechtěnou propojenost (coupling) napříč nesouvisejícími třídami.

Falleri prozíravě připravil abstrakci `Client`, kterou prozatím implementovali pouze rozdílové příkazy `AnnotatedDiffClient`, `WebDiff` a další. Přidal jsem novou implementaci `MergeClient`. V té se ověřuje, že uživatel předal argumentem tři soubory a vzniká instance třídy `MergeMapping`, která mapuje uzly mezi bází a oběma stranami. Projekt GumTree implementuje tři mapování: gumtree (výchozí), change-distiller a xy matcher.

Původní implementace podporuje parsování řady jazyků včetně c, ruby a json. Obsahuje i parser pro PHP, ale pro dávno nepodporovanou verzi 5.2, jejíž vývoj byl oficiálně ukončen v lednu 2011. Navíc je v GumTree využita knihovna pro generování parserů \textsc{antlr} ve staré verzi. Nové gramatiky jsou dostupné pouze pro \textsc{antlr} verze 4 \cite{antlr:homepage}. Část úsilí jsem vložil do hledání vhodné gramatiky pro aktuální php 7.1 a našel jsem open source implementaci od Ivana Kochurkina~\cite{phpgrammar}. Nepodporuje bohužel všechny varianty, co oficiální PHP parser; nefungují například typové nápovědy pro třídy v deklaraci funkce. Jediný vhodnější parser o kterém jsem se dozvěděl je použit v IDE od firmy JetBrains (jako jsou například IDEA a PhpStorm). Jejich implementace php parseru je ale proprietární a nelze ji pro tuto práci použít.

Po zvolení knihovny a naprogramování této práce zveřejnila firma Microsoft nový PHP parser \cite{msft-tolerant}. Přestože zatím nepodporuje celou řadu syntaktických obratů, jde do budoucnosti o nejslibnější knihovnu. Jde o takzvaný \textit{fuzzy parser}, který dokáže do jisté míry pracovat i s nekompletními a syntakticky nevalidními soubory. To by pro tuto práci jako rozšíření verzovacího nástroje bylo velmi vhodné.

Analogicky k existujícímu \textsc{antlr} modulu jsem připravil nový samostatný modul \textsc{antlr4}. Díky tomu lze nyní gumtree velmi snadno rozšířit podporu jakéhokoliv jazyka, pro který existuje \textsc{antlr4} předpis. Tyto změny byly zapracovány do pull requestu a odeslány původnímu autorovi ke komentáři a začlenění\footnote{\url{https://github.com/GumTreeDiff/gumtree/pull/47}}.

Zpětně jsem bohužel zjistil, že přestože je tato gramatika udržovaná a aktualizovaná, podporuje pouze funkce do PHP 5.6. Jediné mě známé parsery, která jsou vždy aktuální, jsou tyto dvě: AST zabudované přímo v jádru PHP, které lze zpřístupnit pomocí rozšíření \code{php-ast} \cite{nikic:phpast}. Druhý parser je napsaný přímo v PHP \code{php-parser} \cite{nikic:parser}. Oba projekty spravuje Nikita Popov, jeden z členů PHP internals, kteří se starají o vývoj jazyka. Lepší postup než rozšiřovat GumTree – jak se zpětně ukázalo – by bylo nechat parsování na PHP, přetransformovat tyto struktury do XML, a ty pomocí GumTree mapovat.

## Ukázka výstupu

\begin{listing}
  \begin{subfigure}{.33\textwidth}
    \begin{minted}{php}
      <?php
      return 1+1;
    \end{minted}
    \subcaption{Společná báze.}
  \end{subfigure}%
  \begin{subfigure}{.33\textwidth}
    \begin{minted}{php}
    <?php
    return '2' +1;
    \end{minted}
    \subcaption{První změna.}
  \end{subfigure}%
  \begin{subfigure}{.33\textwidth}
    \begin{minted}{php}
    <?php
    return 1 + four();
    \end{minted}
    \subcaption{Druhá změna.}
  \end{subfigure}
  \caption{První a druhá změna lze spojit, protože každá ovlivňuje jiný nekolidující token.}
\end{listing}

\begin{listing}
  \begin{minted}{php}
  <?php
  return '2' + four();
  \end{minted}
  \caption{Výsledný kód ze spojených změn. Za povšimnutí stojí i správně spojené změny ve whitespace.}
\end{listing}

V tradičním řádkovém algoritmu by tyto změny kolidovaly a vyústily v kolizi. Implementovaný algoritmus pracující nad AST změny dokáže korektně začlenit.


## Problémy implementace

\begin{listing}
  \begin{minted}{php}
  <?php
  function add($a, $b) {
    return $a + $b;
  }
  \end{minted}
  \caption{Společná báze.}
\end{listing}

\begin{listing}
  \centering
  \begin{subfigure}{.5\textwidth}
    \begin{minted}{php}
    <?php
    function add($alpha, $beta) {
      return $alpha + $beta;
    }
    \end{minted}
  \end{subfigure}%
  \begin{subfigure}{.5\textwidth}
    \begin{minted}{php}
    <?php
    function add($a, $b) {
      return $a->add($b);
    }
    \end{minted}
  \end{subfigure}
  \caption{První a druhá změna, každá ovlivňující jiný nekolidující token.}
\end{listing}

\begin{listing}
  \begin{minted}{php}
  <?php
  function add($alpha, $beta) {
    return $a->add($b);
  }
  \end{minted}
  \caption{Výsledný nefunkční kód se špatným mapováním.}
\end{listing}

\begin{figure}
  \begin{adjustwidth}{-1.5in}{-1.5in}
    \centering
    \includegraphics[width=1.3\textwidth,scale=0.5]{images/bp-ast-matching-error}
  \end{adjustwidth}
  \caption{Zjednodušené AST z ukázky rozbitého mapování. Zelenou tlustou čarou jsou znázorněna mapování mezi listy. Uzly na vyšší úrovni jsou také namapované, ale pro přehlednost byly spoje vynechány. Zelené uzly v pravém stromu nejsou na nic namapované a budou se chovat jako nově vložené uzly. Protože červené uzly nemají mapování v pravém stromu, budou označeny jako smazané.}
\end{figure}

Možná řešení tohoto problému jsou tři: upravit algoritmus mapování GumTree, upravit AST aby současný GumTree fungoval podle očekávání, a nebo zavést složitější systém zámků, který by nalezl související uzly (v ukázce kódu například \code{\$a} v definici parametrů a ve volání funkce). Částečným řešením je i vyhazování kolize při detekování ztráty změn (zde ztráta změn v uzlu \code{Addition Context}), kterou jsem popisoval u algoritmu \code{createNode} \hyperref[merge-ztrata-zmen]{v sekci}.

Rozhodl jsem se upravit AST a uchovávat všechny kontextové uzly. Tím se tento problém vyřešil a merge algoritmus funguje správně. Příklad rozšířeného AST je \hyperref[ukazka-ast]{v příloze}.

\begin{listing}
  \begin{minted}{php}
  <?php
  function foo($alpha, $beta) {
    return $alpha->add( $beta);
  }
  \end{minted}
  \caption{Výsledný správný kód po rozšíření AST.}
\end{listing}


## Výkon a porovnání s Myers

Následující tabulka obsahuje porovnání, jak dlouho trvá implementaci AST merge resp. Myers merge spojit zadané soubory. Baseline je test proti prázdným souborům a obsahuje například parsování vstupních argumentů, příprava pomocných struktur a především v případě Javy inicializace virtuálního stroje.

\begin{center}
 \begin{tabular}{||l c c c||}
 \hline
  & baseline [s] & 4K soubory [s] & 50K soubory [s] \\ [0.5ex]
 \hline\hline
 AST merge   &    1.009 &      1.264 &      \hspace{-1ex}11.122 \\
 \hline
 Myers merge &    0.052 &      0.054 &      0.068 \\
 \hline
\end{tabular}
\end{center}

Podle očekávání je AST merge aplikace v Javě řádově pomalejší, než roky optimalizovaná implementace Myersova řádkového porovnávání. Zajímavé na těchto výsledcích je především to, že i se stávající implementací lze AST merge běžně používat. Dále toto porovnání bere v potaz čistě procesorový čas; hlavní motivací pro AST merge bylo vyřešit více konfliktů a ušetřit tím uživateli čas strávený manuální úpravou, čehož bylo dosaženo.

Dílčí části algoritmu mají časovou komplexitu $O(n)$, pouze parser běží v patologických situacích v $O(n^4)$. Vysoký čas běhu aplikace je tedy pravděpodobně způsoben nedokonalou implementací, nikoliv špatným návrhem merge algoritmu.
