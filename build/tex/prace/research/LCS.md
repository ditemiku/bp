### Nejdelší společná podsekvence
\label{lcs}

V originále *LCS*, *Longest common subsequence*.

Tento problém a jeho algoritmické řešení jsou základem řádkového porovnávání souborů. Je~využit například v Unixovém nástroji `diff`~\cite{Hunt1975}, od kterého se odvíjí složitější porovnávací a verzovací algoritmy. LCS je speciální případ Problému editační vzdálenosti (často také zvaný konkrétně Levenshteinova vzdálenost)~\cite{Masek1980}.

Formálně je problém poprvé definován Hirschbergem takto~\cite{Hirschberg1977}: řetězec $c = c_1 c_2 \cdots c_p$ je *podsekvence* řetězce $a = a_1 a_2 \cdots a_m$ právě tehdy, pokud existuje přiřazení $F: \left\{1, 2, \ldots, p\right\} \rightarrow \left\{1, 2, \ldots, m\right\}$ takové, že $F(i) = k$ pouze pokud $c_i = a_k$ a $F$ je monotóní striktně rostoucí funkce. Odstraněním $m - p$ symbolů (ne nutně po~sobě jdoucích) z $a$ vznikne $c$. Příkladem podsekvence řetězce `fabrika` jsou `frk` nebo `brka`.

\newcommand*{\Ss}{\ensuremath{\mathrm{Ssq}}}

Funkce $\Ss(c)$ je definována jako zobrazení řetězců do množiny řetězců, kde pro parametr $c$ funkce $\Ss$ vrací množinu všech podřetězců $c$.\footnote{První výskyt množinové definice jsem nalezl v Maier~1978~\cite{Maier1978} a dále v Hsu a Du 1982~\cite{Hsu1984}. Hirschberg v své práci~\cite{Hirschberg1977} tuto definici ještě nepoužívá.}

Společná podsekvence $c$ řetězců $a$, $b$ je právě taková podsekvence, která je zároveň podsekvence $a$ a podsekvence $b$. Formálně $\Ss(a, b) = \Ss(a) \cap \Ss(b)$. Definuji obecnou množinovou variantu $\Ss(A) = \bigcap\limits_i \Ss(A_i)$, kde $A$ je množina řetězců.

Nejdelší společná podsekvence je množina všech společných podsekvencí, pro které neexistuje žádná delší podsekvence. Formálně:

\vspace{-2em}
\begin{align*}
\mathrm{LCS}(A) = \left\{c \in \Ss(A); \nexists\ c_{longer} \in \Ss(A) : \left|c_{longer}\right| > \left|c\right| \right\}
\end{align*}

kde $A$ je množina řetězců a $\left|c\right|$ značí počet znaků řetězce $c$.

\label{ynlcs}
Maier \cite{Maier1978} zavádí problém *Yes/No LCS* (*YNLCS*).\newline
Existuje pro dané $k \in \mathbb{N}$ a množinu řetězců $R$ aspoň jedno $c$ z $LCS(R)$ takové, že $|c| >= k$? Formálně:

\vspace{-2em}
\begin{align*}
  \textrm{YNLCS}(R, k) = \begin{cases}
      1 & \exists\ c \in \mathrm{LCS}(R) : |c| \ge k \\
      0 & \mathrm{jinak}
   \end{cases}
\end{align*}
kde $R$ je množina řetězců nad stejnou abecedou a $k \in \mathbb{N}$ je požadovaná minimální délka podsekvence.

Obecné řešení YNLCS problému -- pro $R$ s velikostí větší než 2 -- má třídu složitosti NP-úplné \cite{Maier1978}. Pro tuto práci to znamená, že nelze efektivně přímo využít LCS pro více velkých vstupů (větev 1, společný základ, větev 2).

### Algoritmické řešení LCS

Podle *A survey of LCS algorithms* \cite{Bergroth2000} se typicky tento problém neřeší obecně. Zjednodušuje se na dva vstupní řetězce $\mathrm{LCS}\left(\left\{a, b\right\}\right)$, nebo na Nejdelší rostoucí podposloupnost, kterou se ale tato práce dál nezabývá, protože nemá v kontextu verzování praktické využití.

Nejrychlejší známý algoritmus od Masek a Paterson \cite{Masek1980} má časovou složitost $O(n^2/\log(n))$, ale vyžaduje řetězce nad konečnou abecedou.
\iffalse
Pro porovnávání řádků textu, kde abecedou jsou celé řádky, není tento předpoklad splněn.\footnote{Pokud uměle neomezíme maximální délku řádku, vždy lze sestrojit delší řádek přidáním dalšího znaku. Vstupní abeceda při tomto použití tedy není konečná.}
TODO what, není to blbost?
\fi

### Naivní řešení

\begin{listing}[H]
  \begin{algorithmic}
    \State $A_i :=$ $i$. řádek prvního řetězce $\mathcal{A}$
    \State $B_j :=$ $j$. řádek druhého řetězce $\mathcal{B}$
    \State $P(i,j) :=$ délka LCS do $i$. řádku $\mathcal{A}$ a $j$. řádku $\mathcal{B}$
    \Function{$P$}{$i,j$}
      \If{$i = 0 \lor j = 0$}
        \State \Return $0$
      \ElsIf{$A_i = B_j$}
        \State \Return $1 + P(i-1, j-1)$
      \Else
        \Comment $A_i \ne B_i$
        \State \Return $\mathrm{max}\large(P(i-1,j),P(i,j-1)\large)$
      \EndIf
    \EndFunction
  \end{algorithmic}
\end{listing}

Tento algoritmus běží s časovou i prostorovou náročností $O(mn)$, kde $m$ je počet řádků $\mathcal{A}$ a $n$ počet řádků $\mathcal{B}$.

\iffalse
\begin{figure}
  \includegraphics{images/lcs-recursion}
  \caption{Ilustrace Huntova algoritmu. Převzato z Wiki Commons \cite{wiki:hunt}.}
\end{figure}
\fi


### Hunt–McIlroy algorithm - Essential Matches

Hledá nezbytné shody (\textit{essential matches}) neboli $k$-kandidáty (\textit{$k$-candidates}), které definuje jako pár $\{i,j\}$ pro který platí, že
$A_i = B_j$ a současně\newline
$P(i,j) > \mathrm{max}(P(i-1,j),P(i,j-1))$. Efektivita tohoto řešení vychází z toho, že výsledné LCS může být tvořeno pouze $k$-kandidáty. \cite{Hunt1975}

\begin{figure}[H]
  \includegraphics{images/k-candidates-diagram}
  \caption{Znázornění redukce možných cest pro LCS řetězců \code{aaba} a \code{abaa}. Černé body a čáry označují postup naivního řešení. Červené body označují $k$-kandidáty. Červenou čarou je zvýrazněno jedno z možných řešení. Ilustrace převzata z Wiki Commons \cite{wiki:k-candidate}.}
  \label{image-lcs}
\end{figure}

Originální pseudokód je publikován v \textit{An algorithm for differential file comparison}~\cite[strana~3]{Hunt1975}. Má časovou složitost $O(n \log m)$, prostorovou $O(mn)$. Ve všech ohledech je překonán Myersovým algoritmem.

### Myers
\label{myers}

Myersův algoritmus dále rozšiřuje Huntův algoritmus.

Myers definuje $N$ jako součet délek vstupních řetězců a $D$ jako délku nejkratšího editačního skriptu. $L$ je délka LCS a platí tedy $D = 2(N - L)$.
Jeho algoritmus je vystaven na předpokladu, že $O(ND)$ je pro typické použití časově efektivnější, než $O(NL + NlogN)$ \cite{Myers1986}.

Tento iterativní algoritmus je postaven na úvaze, že nejsložitěji se k výsledku můžeme dobrat v $m+n$ operacích, kde $m$ a $n$ jsou délky vstupů, a to pokud $n$ znaků smažeme a $m$ přidáme (\textsc{búno}). Toto je ilustrováno \hyperref[image-lcs]{na obrázku}. Aby vznikl co nejmenší výstup, je potřeba použít shodné znaky v obou vstupech: to znázorňují diagonální přechody v diagramu. Algoritmus pracuje s tabulkou přechodů, kde si každá buňka pamatuje odkud se na ni došlo a na~jaké pozici v LCS se lze dostat. Postupně se od nejkratší vzdálenosti prochází možnosti „pohyb~vpravo“ a „pohyb~dolů“ a vyplňuje se tabulka přechodů. Existuje-li diagonální přechod, jako cíl přechodu se vyplní až poslední diagonála (příkladem je přechod \code{[1,0] - [4,2]}). Pokud se při průchodu zjistí, že buňka je již vyplněna a obsahuje nižší cílovou pozici, je přepsána. Ukázka je \hyperref[image-myers-lcs]{na diagramu}.

\begin{figure}[H]
  \begin{adjustwidth}{-1.5in}{-1.5in}
    \centering
    \includegraphics[width=1.3\textwidth]{images/bp-myers}
  \end{adjustwidth}
  \caption{Myersův algoritmus aplikovaný na vstupy \code{abca} a \code{cbab}. Na pravé straně je tabulka přechodů. Odpovídající hrany jsou barevně označeny.}
  \label{image-myers-lcs}
\end{figure}

Pseudokód je v publikovaném článku \cite{Myers1986}.

#### Porovnávání hashů řádků

Hunt doporučuje v kapitole \textit{3. Hashing} neporovnávat přímo řádky, ale jejich hashe~\cite{Hunt1975}. To může vést na nalezení falešně pozitivní shody. Pro hashe s adekvátně velkým výstupy je to ale nepravděpodobné, a po nalezení výsledku lze tyto shody odfiltrovat.
