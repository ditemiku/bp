## Využití LCS v algoritmu \code{diff}

### Hunt–McIlroy
\label{hunt-mc}

Poprvé publikován jako \textit{An algorithm for differential file comparison} \cite{Hunt1975}, tento algoritmus je rozšířením řešení LCS.

Staví nad dynamickým řešením porovnávající dva soubory s $m$, resp. $n$ řádky, které běží v čase $O(mn)$ a má $O(mn)$ prostorovou náročnost.

Tento algoritmus je veskrze překonán algoritmem Myersovým.
\iffalse
Článek odkazuje na Hirschbergovo \cite{Hirschberg1977} zlepšení
TODO dopsat větu
\fi

### Myers

Jako diff lze rovnou použít Myersův LCS algoritmus popsaný \hyperref[myers]{v kapitole}. Při vyplnění tabulky přechodů je získána i cesta, kterou se k LCS došlo. Pohyb vpravo reprezentuje smazání symbolu (typicky řádku) z prvního vstupu, pohyb dolů odpovídá vložení symbolu z druhého vstupu. V literatuře se pojem Myers LCS a Myers diff volně zaměňuje.
