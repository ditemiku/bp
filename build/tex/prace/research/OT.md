## Operační transformace
\label{ot}

Operační transformace (OT) jsou od základu odlišný způsob začleňování více změn, než tradiční řádkové algoritmy popsané výše. Jejich snahou je reprezentovat uživatelův záměr, nikoliv výsledný stav \cite{Sun1998}. Systém například pracuje s informací, že odstavec byl přesunut mezi jiné dva odstavce \cite{Chawathe1996}. Tradiční diff algoritmy znají pouze operace \textit{smazat} a \textit{vložit}.

Při začleňování změn se přehrají všechny změny seřazené podle kauzality.

Příklad:
Při počátečním textu \code{ahoj světe}, uživatel A upravil první slovo na \code{dobrý den} (celkem 9 znaků místo původních 4). Druhý uživatel nezávisle na prvním upravil druhé slovo na \code{nebe} (změnil tedy znaky 6 až 10). Při začleňování změn se nejprve změny seřadí; zde vyberu změny od A jako první, ale protože na sobě tyto změny nezávisí, je to bez újmy na obecnosti. Změnu od uživatele B je nezbytné před aplikací upravit. Místo původního rozmezí 6-10 upravuji nyní rozmezí 11-15 v textu \code{dobrý den světe}. Po aplikace této upravené změny vznikne výsledný společný text \code{dobrý den nebe}. Pokud by byly změny uživatele B aplikovány bez transformace, vznikl by (špatně) text \code{dobrý nebe}, což rozhodně nebyl záměr uživatelů.

### Problém konkurenčních změn (\textit{Concurrency Control Problem})

Některé kolize nelze vyřešit vůbec (například pokud jeden uživatel přidá do věty slovo a druhý uživatel větu nezávisle na první úpravě větu smaže, nebo oba upraví stejné slovo).

Původní článek z roku 1989 \cite{Ellis1989} uvažuje jeden sdílený systém, který uživatelé mají replikovaný na terminálech. Nabízí řešení jako je zamykání dat před jejich modifikací a transakce. Pro tuto práci jsou vhodnější další zmíněná řešení:

\begin{adjustwidth}{0.2in}{0.2in}
  \begin{itemize}
  	\item Detekování závislosti (\textit{dependency detection}), při čemž se kolize rozpoznají podle časového razítka a ručně se řeší uživatelem.
  	\item Vracení změn (\textit{reversible execution}), kde se při identifikaci změn mimo pořadí úpravy vrátí a přehrají v pořadí správném.
  \end{itemize}
\end{adjustwidth}

Původní návrh systému využívající OT \cite{Ellis1989} definoval OT-systém jako korektní, právě pokud splňuje tyto dvě vlastnosti:
\begin{adjustwidth}{0.2in}{0.2in}
  \begin{itemize}
  	\item Zachování kauzality (\textit{causality preservation}): pokud si dvě změny $a, b$ předchází, tak u všech uživatelů se nejprve provede $a$ a až pak $b$. Pro řazení se používá definice běžná v distribuovaných systémech zavedená Lamportem \cite{Lamport1978}.
  	\item Konvergence: po aplikaci všech změn u všech klientů je výsledek identický.
  \end{itemize}
\end{adjustwidth}

Sun \cite{Sun1998} tento model dále rozšiřuje o zachování záměru (\textit{intent-preservation}).

\begin{figure}
  \includegraphics[width=0.7\textwidth,scale=0.3]{images/bp-ot-overlap}
  \caption{Nepřekrývající se (vlevo) a překrývající se operace. Svislé čáry $C1$ a $C2$ reprezentují klienty/uživatele. Přeloženo z článku \textit{Concurrency Control in Groupware Systems} \cite{Ellis1989}.}
\end{figure}
