### 3DM
\label{3dm}

\textsc{3dm} \cite{Lindholm2001} je zkratkou pro \textit{3-way merging, Differencing and Matching}. Jde o nejstarší mně známou práci, která se třícestnému merge věnuje. Algoritmy ChangeDistilling a GumTree (viz dále) popisují pouze diff a generování editačního skriptu.

Přestože práce primárně popisuje spojování XML dokumentů, lze většinu konceptů a algoritmů použít i na AST.

V \textit{The Tree Matching Algorithm} \cite[6. kapitola]{Lindholm2001} Lindholm představuje nový algoritmus pro mapování uzlů mezi podobnými stromy.
