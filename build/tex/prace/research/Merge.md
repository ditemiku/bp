Definuji jednoznačně merge algoritmus jako funkci, která dostává tři řetězce ze stejné abecedy (významem jsou to zdrojový kód $a$, zdrojový kód $b$ a společná báze). Výstupem je jeden řetězec, který zahrnuje změny z obou zdrojových kódů, případně chyba, pokud změny spojit nelze.

\clearpage
V dokumentaci verzovacích nástrojů je často jako merge označován složitější algoritmus, který hledá společné předky, identifikuje tzv. cherry-picks, … a nakonec používají merge podle mé definice. V těchto případech na to je v práci upozorněno a je zdokumentován pouze relevantní kód.
