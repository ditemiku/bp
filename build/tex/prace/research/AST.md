## Abstraktní syntaktický strom
\label{ast}

AST je uspořádaný zakořeněný strom (\textit{ordered rooted tree}). Má tedy jeden význačný kořen (uzel reprezentující zdrojový soubor) a záleží na pořadí potomků: je rozdíl mezi potomky \code{[1,$\div$,2]} a \code{[2,$\div$,1]}.

Jde o hierarchickou reprezentaci zdrojového kódu.

Podoba AST typicky záleží na parseru, který ho generuje. Například gramatika pro jazyk PHP \cite{phpgrammar} generuje v AST uzel pro každý možný kontext. Jednoduchý příkaz \code{1+1} tak může být reprezentován jedním stromem s desítkami uzlů od \code{StatementContext}, přes \code{NotLeftRecursionExpression} a \code{ChainExpression} až po finální context operátoru. Takové AST je velmi nepřehledné, ale výhodné pro automatické zpracování. Této podoby PHP AST využívá merge algoritmus \hyperref[spojovani-ast]{z kapitoly}.

\begin{figure}[H]
  \includegraphics[width=\textwidth]{images/bp-ast}
  \caption{Zdrojový kód a jeden z mnoha možných AST, který tento kód popisuje.}
\end{figure}
