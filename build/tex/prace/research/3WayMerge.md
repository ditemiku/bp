\iffalse
## Třícestný merge

Jinak také třísměrné spojení, \textit{3-way merge}.

na úrovni souborů, resp. git objektů, ale to jsou typicky soubory.

| případ | předek    | head   | remote | výsledek |
|--------|-----------|--------|--------|----------|
|  4     | žádný     | A      | b      | no merge |
|  5ALT  | libovolný | A      | A      | A        |
| 11     | předek    | A      | b      | no merge |
| 13     | předek    | A      | předek | A        |
| 14     | předek    | předek | b      | b        |
| 16     | A nebo b  | A      | b      | no merge |

Tabulka částečně převzatá z git dokumentace Three-way merge \cite{src:git:TrivialMerge}. Byly vynechány případy, kde je je head nebo remote prázdný. Tyto případy nejsou pro tuto práci zajímavé\footnote{Git v interním merge programu \code{read-tree} buď skončí jako \code{no-merge}, nebo s přepínačem \code{--aggresive} soubory z výsledného indexu smaže}.

Výsledek `head` nebo `remove` jsou  `no merge` znamená, že automaticky nelze `head` a `remote` spojit.

\code{5ALT} popisuje mj.~stav, kdy předek je prázdný a `head` i `remote` přidali identický soubor.

Merge je symetrický; nezáleží na tom, jestli mergujeme `head` na `remote` nebo obráceně. Liší se pouze umístěním výsledku. TODO quote http://stackoverflow.com/a/3783067/326257
Díky tomu jsou případy 13 a 14 převeditelné pouze cyklickou záměnou.

TODO wtf is case 16‽

Všechny případy `no merge` triviální merge neřeší a přenechává je k dalšímu zpracování jinými algoritmy.
\fi
