## FastMatch

Záměrem tohoto algoritmu je rozšířit \hyperref[myers]{Myersův algoritmus} o podporu strukturovaných dat, které v teorii grafů definujeme jako uspořádaný strom (což je například AST). K tradičním operacím vložení~(\textit{insert}) a smazání~(\textit{delete}) přidává dvě nové operace: úprava~(\textit{update}) a přesun podstromu~(\textit{subtree move}).

Nepředpokládají se unikátní identifikátory uzlů napříč verzemi souborů: v přípravném kroku je potřeba mezi vstupními stromy uzly namapovat (\textit{matching}). To vede na tzv. \textit{Good Matching Problem}.

Pro diff algoritmus se dále řeší \textit{Minimum Conforming Edit Script Problem}, kde -- stejně jako u tradičních řádkových algoritmů -- se snažíme minimalizovat velikost editačního skriptu, který jeden vstupní strom převede na druhý.

Kompletní algoritmus je v článku \textit{Change Detection in Hierarchically Structured Information} \cite[figure 8, 9]{Chawathe1996}.

Logika podle které lze uzly $a, b$ namapovat je následovná:
\begin{listing}[H]
  \begin{algorithmic}
  \Function{match}{$a,b$}
    \If{type $a \ne$ type $b$}
      \State
      \Return false
    \EndIf

    \If{$a, b$ jsou listy}
      \State
      \Return \Call{podobnost}{hodnota $a$, hodnota $b$} $\ge f$
    \Else
      \State $spolecne \leftarrow \{a \cap b\ :\ $\Call{match}{$a,b$}$\}$
      \State
      \Return $\frac{|spolecne|}{\max(|a|,|b|)} \ge t$
    \EndIf
  \EndFunction
  \end{algorithmic}
\end{listing}

Zápisem $|x|$ se rozumí počet potomků. Konstanty $f$ a $t$ lze volit libovolně.
