### libxdiff

Knihovnu vytvořil Davide Libenzi \cite{xdiff} a pro tuto práci je obzvlášť zajímavá, protože obsahuje celou řadu nových přístupů a řešení. V této knihovně jsou pokročilé diff algoritmy jako Patience diff (\ref{patience-diff}) a Histogram diff (\ref{histogram-diff}). Tyto algoritmy jsou detailně rozebrány v kapitole věnující se verzovacímu systému Git.
