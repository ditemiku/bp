### Google Diff-Match-Patch
\label{dmp}

Knihovna implementuje Myersův algoritmus. \cite{googledmp}

Zajímavá na této knihovně je optimalizace od Mike Slemmer v `cpp` kódu \cite{src:googledmp:dmpcpp}, která pro složitější porovnávané soubory (alespoň 100 řádků) převede řádky na jednodušší interní strukturu a potom je mapuje zpět.

Další inspirativní optimalizací je oříznutí identického prefixu a postfixu v obou porovnávaných textech \cite{src:googledmp:trim}.
