## Algoritmy v Mercurial

Mercurial využívá \textsc{c} implementaci Pythonové knihovny `difflib` \cite{mercurial:wiki}. Toto je především z historického důvodu, protože samotný Mercurial je naprogramovaný v Pythonu.

Cílem této knihovny je spočítat uživatelsky přívětivý a pohledný diff. Liší se od UNIXového `diff` důrazem na nejdelší \textit{spojitou} a „nezašpiněnou“ (whitespace,~\ldots) podsekvenci. Windowsový nástroj `windiff` má jinou zajímavou vlastnost a to seskupování jedinečných elementů z každé věty. Tyto dva přístupy dohromady dávají intuitivnější výsledek. (Volně přeloženo z komentářů originálního zdrojového kódu \cite{difflib}.)

Algoritmus použitý v \code{difflib} má $O(N^2)$ časovou složitost, kde $N$ je počet vstupních řádků \cite{difflib:src:113}.
