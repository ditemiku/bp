### GNU diff

Nástroj \code{diffutils} \cite{gnu:diff3:index} je vznikl jako první. Implementací vytvořil McIlroy společně s Huntem (\ref{hunt-mc}).

Později správci \code{diffutils} přešli na Myersovu alternativu.

V současné době není tato knihovna – s výjimkou své rozšířenosti – dobrá volba. Neobsahuje žádné pokročilé algoritmy a slouží pouze jako základ či záložní algoritmus (\textit{fallback}) pro ostatní programy.
