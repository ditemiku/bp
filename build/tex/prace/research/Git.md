## Algoritmy v Git
\label{git}

Podobně jako SVN i Git má zmatek v názvosloví a jako merge označuje celou řadu algoritmů. Takzvané \textit{merge strategies} \cite{doc:git:MergeStrategies} označují způsoby, jak nalézt samotné řetězce, nad kterými se 3-way-merge bude volat. Analogie v SVN je SymmetricMerge.

Na rozdíl od SVN lze v Gitu změnit (nastavením parametru \code{--diff-algorithm=}), jaký 3-way-merge se interně má použít. Podle současné dokumentace (2.11) jsou k dispozici tyto algoritmy: myers, minimal, patience a histogram.

První implementace Gitu pouze vytvářela nový systémový proces `diff` \cite{src:git:firstDiffImpl}. Toto typicky volalo GNU implementaci.

Git nyní používá knihovnu `libxdiff`, která je jiná než GNU diff.

### Myers diff

Výchozí nastavení Gitu. Snaží se optimalizovat editační vzdálenost změn. Algoritmus je implementován přesně podle Myersova článku \cite{Myers1986}, popsaného \hyperref[myers]{v kapitole}.

\iffalse
### Minimal diff

Snaží se optimalizovat velikost patche (a hlavně zmen? TODO)

TODO explain change
TODO add example
\fi

### Patience diff (Bram's diff)
\label{patience-diff}

K tomuto algoritmu neexistuje žádný publikovaný článek a Bram Cohen (mj.~autor BitTorrent protokolu) ho pouze představil na svém blogu \cite{cohen:announcement} a později lehce zdokumentoval \cite{cohen:patiencediff}. Vznikl implementací ve verzovacím nástroji Bazaar a o dva roky později byl přidán do Gitu \cite{git:patience:commit} skrze knihovnu \code{libxdiff}.

\parbox{.85\textwidth}{
\begin{enumerate}
\item Dokud jsou odshora řádky identické na levé a pravé straně, posun o řádek dolů.
\item Dokud jsou odspodu řádky identické na levé a pravé straně, posun o řádek nahoru.
\item Pro všechny unikátní řádky (takové co se na každé straně vyskytují právě jednou) se vytvoří LCS.
\item Kroky 1-2 se opakují pro každou sekci mezi namapovanými řádky.
\end{enumerate}
}

Předností tohoto algoritmu je seskupování společných změn, což Myersův algoritmus nedělá.

Na testovací sadě o velikosti 3000 běžel Myers diff 1.93 (1.75+0.17) sekund a Patience diff 2.25 (2.07+0.16) sekund. Patience tedy není výrazně pomalejší a je vhodný pro praktické použití. Výsledky jsou převzaty z mailing listu Gitu~\cite{git:ml:perf}.

\begin{listing}[H]
  \centering
  \begin{subfigure}{.5\textwidth}
    \begin{minted}{diff}
diff --git a/a.php b/b.php
index f002945..bd6a48b 100644
--- a/a.php
+++ b/b.php
@@ -1,11 +1,11 @@
 <?php

-function beta()
-{
-       return cos(time());
-}
-
 function alpha()
 {
        return sin(time());
 }
+
+function beta()
+{
+       return cos(time());
+}
    \end{minted}
    \caption{Ukázkový výstup Patience diff}
  \end{subfigure}%
  \begin{subfigure}{.5\textwidth}
    \begin{minted}{diff}
diff --git a/a.php b/b.php
index f002945..bd6a48b 100644
--- a/a.php
+++ b/b.php
@@ -1,11 +1,11 @@
 <?php

-function beta()
+function alpha()
 {
-       return cos(time());
+       return sin(time());
 }

-function alpha()
+function beta()
 {
-       return sin(time());
+       return cos(time());
 }
    \end{minted}
    \caption{Myers diff}
  \end{subfigure}
  \caption{Rozdíl mezi výstupem Patience diff (vlevo) a Myers diff. Levý diff lépe reprezentuje záměr změny: přesunutí funkce \code{beta} za \code{alpha}.}
\end{listing}

### Histogram diff
\label{histogram-diff}

Jde o rozšiřující implementaci Patience diff, původně v Jgit~\cite{jgit:histogram:src} a později v Gitu~\cite{git:histogram:commit}.

\thispagestyle{empty}
\begin{enumerate}
  \item Pro levou stranu se vytvoří histogram výskytů jednotlivých řádků.
  \item Pro každý řádek z pravé strany se najde počet výskytů v předpočítaném levém histogramu.
  \item Vybere se taková LCS, která má celkem v levé straně nejmenší počet výskytů.
  \item Kolem LCS se vstup rozdělí a rekurzivně se algoritmus volá pro sekce před a po LCS.
\end{enumerate}

Výstup je často stejný jako u Patience diff. Rozdíl může nastat pouze pokud se v kódu vyskytuje více neunikátních řádků. Hlavním rozdílem oproti Patience je lepší výkon. Podle stejného měření jako výše běžel Histogram diff 1.90~(1.74+0.15) sekund, tedy dokonce o něco kratší dobu, než Myers diff.

### Heuristiky

Kromě samotných diff algoritmů ovlivňují přehlednost výstupu tzv.~\textit{sliders}~(posuvníky). Jde o situace, kde kraje editačního skriptu lze cyklicky posouvat, například u složených závorek mezi těly metod. Situace je přehledně viditelná na~\hyperref[slider-example]{ukázce}. \newline
Git od verze~2.11 implementuje postprocessing heuristiku, která správně opraví~97,8~\% těchto situací~\cite{git:slider}.

\begin{listing}
  \centering
  \begin{subfigure}{.5\textwidth}
    \begin{minted}{diff}
@@ -2188,12 +1996,6 @@
                return dir->nr;

        /*
-        * Stay on the safe side.
-        * if read_directory has
-        */
-       clear_sticky(dir);
-
-       /*
         * exclude patterns are
         * create_simplify
         * subset of positive
    \end{minted}
  \end{subfigure}%
  \begin{subfigure}{.5\textwidth}
    \begin{minted}{diff}
@@ -2188,12 +1996,6 @@
                return dir->nr;

-       /*
-        * Stay on the safe side.
-        *  if read_directory has
-        */
-       clear_sticky(dir);
-
        /*
         * exclude patterns are
         * create_simplify
         * subset of positive
    \end{minted}
  \end{subfigure}
  \caption{Ukázka špatně zarovnaného editačního skriptu (vlevo) a posunutého skriptu o jeden řádek. Výstup vpravo je uznávaný jako čitelnější. Oba skripty nicméně vedou na identický výsledek.}
  \label{slider-example}
\end{listing}
