### ChangeDistilling
\label{ChangeDistilling}

Jde o první článek \cite{Fluri2007}, ve kterém se přístup FastMatch aplikuje na zdrojový kód. Přínosy tohoto článku jsou mj. vytvoření testovací sady, téměř 45 % zrychlení oproti FastMatch a publikace programu pro porovnávání na úrovni uzlů AST.

Pro adaptaci na AST se odstraňuje předpoklad, že v pravém stromu je maximálně jeden list, který se může namapovat na odpovídající list v levém stromu~\cite[kapitola 2.2.2]{Fluri2007}. \textit{ChangeDistilling} funguje správně pouze na AST LaTeX dokumentu, který má typicky hodně veliké listy a málo uzlů. Pro obecné AST není \textit{ChangeDistilling} dobře použitelný.
