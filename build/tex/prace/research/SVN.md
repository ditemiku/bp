## Algoritmy v Apache Subversion

SVN je zástupcem Client-Server verzovacího modelu. Repozitář s kódem existuje pouze jeden – centrální – a bez připojení k němu nelze vytvářet nové revize/commity.
Situace vyžadující merge nastane při explicitním požadavku na spojení dvou větví.

Do verze 1.5 SVN používalo pouze 2-way merge, protože chyběly meta-informace pro nalezení společného předka. Po přidání \textit{Merge tracking} \cite{changelog:svn:MergeTracking} byl v repozitáři každý merge zaznamenán.

Ve vyšších verzích jsou v `mergeinfo` k dispozici informace o poslední společné revizi, které jsou pro automatické řešeni kolizí a 3-way merge nezbytné.

### SymmetricMerge

SVN v dokumentaci tvrdí, že používá algoritmus SymmetricMerge \cite{wiki:svn:SymmetricMerge}. Ten interně volá metodu `three_way_merge(m.base, m.tip, m.target)`, ke které se mi nepodařilo najít dokumentaci ani zdrojový kód. Podle signatury jde ale o funkci merge, která odpovídá definici v této práci. Podle vývojáře Subversion je tato funkce přímo převedena na volání aplikace `diff3` \cite{src:svn:diff3}.

SVN tedy používá `diff3` algoritmus.
