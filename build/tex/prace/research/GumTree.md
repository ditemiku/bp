### GumTree
\label{gumtree}

Tento algoritmus dále rozšiřuje ChangeDistilling, popsaný \hyperref[ChangeDistilling]{v kapitole}.\newline
GumTree přidává podporu pro obecné AST (ne pouze AST LaTeX dokumentu).

Mapování se provádí dvoukrokově:
\begin{enumerate}
  \item Top-down fáze: Od kořene se namapují isomorfní uzly (uzly se stejným typem, hodnotou a pouze isomorfními potomky). Mapování z toho kroku je tzv. \textit{anchor mapping}.
  \item Bottom-up fáze: Dva dosud nenamapované uzly se namapují (\textit{container mapping}), pokud obsahují dostatečný počet namapovaných potomků. Pokud se v tomto kroku dva uzly namapují, provede se ještě doplňkový \textit{recovery matching}, při kterém se nenamapovaní potomci zkusí namapovat agresivněji než v prvním top-down průchodu.
\end{enumerate}

\begin{figure}[H]
  \begin{adjustwidth}{-1.5in}{-1.5in}
    \centering
    \includegraphics[width=1.3\textwidth,scale=1.0]{images/gumtree}
  \end{adjustwidth}
  \caption{Ukázka mapování. V top-down fázi se namapují ekvivalentní uzly (znázorněné dlouhou přerušovanou červenou čarou). V bottom-up fázi se namapují podobné uzly (čerchovaná zelená čára) a provede se \textit{recovery mapping} (tečkovaná modrá čára). Nenamapované uzly jsou šedé. Převzato z \textit{Fine-grained and Accurate Source Code Differencing} \cite{Falleri2014}.}
\end{figure}
