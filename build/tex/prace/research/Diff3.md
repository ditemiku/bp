## Algoritmus a program diff3
\label{diff3}

Aplikace, kterou má tato práce za cíl vytvořit, přijímá i data ve formátu `diff3`~\cite{gnu:diff3:merging}. Tento formát byl vybrán především proto, že jej může generovat Git v případě kolize.
\iffalse
\footnote{
  Ve výchozím nastavení Git vypisuje pouze kolizní větve a ne jejich společný základ. Pro výstup ve formátu \code{diff3} je nutné upravit konfigurace příkazem
  \code{git config --global merge.conflictstyle diff3} nebo kolize checkoutnout
  \code{git checkout --conflict=diff3 ...}
}
\fi
Další výhodou toho formátu je, že v jednom souboru obsahuje všechny tři vstupy a uživatel si tedy nemůže splést terminologii v naší aplikaci.
Navíc je vstup kompaktní: nezměněné části obsahuje pouze jednou, což by ale vyřešila i komprese.

Formát `diff3` je výstupem stejnojmenné GNU utility `diff3`, kterou naprogramoval Randy Smith v roce~1988 a byla zveřejněna v rámci balíčku `diffutils`~\cite{gnu:diff3:index}. Tato utilita byla akademicky popsána v roce~2007 v článku \textit{A Formal Investigation of Diff3}~\cite{Khanna2007}.
