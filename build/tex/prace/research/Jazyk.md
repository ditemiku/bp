V dalším textu bude použito následujících termínů~\cite{Chomsky1957}:

\begin{description}
  \item[abeceda~$\Sigma$:] Konečná množina symbolů.
  \item[řetězec nad abecedou:] Konečná posloupnost symbolů abecedy.
  \item[formální jazyk nad abecedou:] Množina všech posloupností\\
  symbolů dané abecedy.
\end{description}
