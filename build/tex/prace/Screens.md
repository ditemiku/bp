\chapter{Ukázky webové aplikace}
\label{ukazky-webu}

Stránka s výsledkem uživatelova požadavku, včetně automaticky vygenerovaného řešení kolize (příloha je na následující straně).

\includepdf{images/web-results.pdf}
