% Implementace {#implementace}

\input{impl/Merge.tex}
\input{impl/Web.tex}
\clearpage
\input{impl/GitPlugin.tex}
