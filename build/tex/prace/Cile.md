Cílem rešeršní práce je nastudovat řádkové merge algoritmy, které implementuje
verzovací systém Git a především zjistit jejich nevýhody oproti navrhované změně,
existující merge algoritmy nad stromovými strukturami,
vypracovat jejich porovnání a vybrat z nich nejvhodnější pro tuto práci;
generování abstraktního syntaktického stromu v PHP; systém zásuvných modulů/rozšíření Git.

Cíl praktické části práce zahrnuje především implementaci samotného
rozšíření pro Git, které rozpozná zdrojové soubory v PHP,
vytvoří jejich abstraktní syntaktické stromy a porovná je
zvoleným algoritmem. Pro konkrétní případy budou uvedeny zdrojové kódy,
které AST přístup řeší oproti řádkovému lépe (případně hůře).
